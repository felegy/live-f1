namespace LiveF1.Core.Model
{
	using System;

	public enum CarPacketType
	{
		CAR_POSITION_UPDATE	= 0,
		CAR_POSITION_HISTORY	= 15,
		LAST_CAR_PACKET
	}
}


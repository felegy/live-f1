namespace LiveF1.Core.Model
{
	using System;

	public enum SystemPacketType
	{
		SYS_EVENT_ID		= 1,
		SYS_KEY_FRAME		= 2,
		SYS_VALID_MARKER	= 3,
		SYS_COMMENTARY		= 4,
		SYS_REFRESH_RATE	= 5,
		SYS_NOTICE		= 6,
		SYS_TIMESTAMP		= 7,
		SYS_WEATHER		= 9,
		SYS_SPEED		= 10,
		SYS_TRACK_STATUS	= 11,
		SYS_COPYRIGHT		= 12,
		LAST_SYSTEM_PACKET
	}
}
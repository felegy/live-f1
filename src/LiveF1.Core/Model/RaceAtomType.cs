namespace LiveF1.Core.Model
{
	using System;

	public enum RaceAtomType
	{
		RACE_POSITION	= 1,
		RACE_NUMBER	= 2,
		RACE_DRIVER	= 3,
		RACE_GAP	= 4,
		RACE_INTERVAL	= 5,
		RACE_LAP_TIME	= 6,
		RACE_SECTOR_1	= 7,
		RACE_PIT_LAP_1	= 8,
		RACE_SECTOR_2	= 9,
		RACE_PIT_LAP_2	= 10,
		RACE_SECTOR_3	= 11,
		RACE_PIT_LAP_3	= 12,
		RACE_NUM_PITS	= 13,
		LAST_RACE_ATOM
	}
}


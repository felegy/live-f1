namespace LiveF1.Core.Model
{
	using System;

	public class CurrentState
	{
		public CurrentState ()
		{
		}

		public string Host { 
			get;
			set;
		}
		
		public string AuthHost {
			get;
			set;
		}
		
		public string Email {
			get;
			set;
		}
		
		public string Password {
			get;
			set;
		}
		
		public string Cookie {
			get;
			set;
		}
		
		public int Key {
			get;
			set;
		}
		
		public int Salt {
			get;
			set;
		}
		
		public int DecryptionFailure {
			get;
			set;
		}
		
		public int Frame {
			get;
			set;
		}

		public int EventNo {
			get;
			set;
		}

		public EventType EventType {
			get;
			set;
		}

		public TimeSpan RemainingTime {
			get;
			set;
		}

		public TimeSpan EpochTime {
			get;
			set;
		}

		public int LapsCompleted {
			get;
			set;
		}

		public int TotalLaps {
			get;
			set;
		}

		public FlagStatus Flag {
			get;
			set;
		}

		public int TrackTemp {
			get;
			set;
		}

		public int AirTemp {
			get;
			set;
		}

		public int Humidity {
			get;
			set;
		}

		public int WindSpeed {
			get;
			set;
		}

		public int WindDirection {
			get;
			set;
		}

		public int Pressure {
			get;
			set;
		}
	}
}


namespace LiveF1.Core.Model
{
	using System;

	public enum FlagStatus
	{
		GREEN_FLAG = 1,
		YELLOW_FLAG,
		SAFETY_CAR_STANDBY,
		SAFETY_CAR_DEPLOYED,
		RED_FLAG,
		LAST_FLAG
	}
}


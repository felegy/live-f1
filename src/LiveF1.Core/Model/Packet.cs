namespace LiveF1.Core.Model
{
	using System;

	public class Packet
	{
		public Packet ()
		{
		
		}

		public int Car {
			get;
			set;
		}

		public int Type {
			get;
			set;
		}

		public int Data {
			get;
			set;
		}

		public int Len {
			get;
			set;
		}

		public char[] payload {
			get;
			set;
		}

	}
}


namespace LiveF1.Core.Model
{
	using System;

	public enum EventType
	{
		RACE_EVENT = 1,
		PRACTICE_EVENT = 2,
		QUALIFYING_EVENT = 3
	}
}


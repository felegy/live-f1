namespace LiveF1.Core.Model
{
	using System;

	public enum PracticeAtomType
	{
		PRACTICE_POSITION	= 1,
		PRACTICE_NUMBER		= 2,
		PRACTICE_DRIVER		= 3,
		PRACTICE_BEST		= 4,
		PRACTICE_GAP		= 5,
		PRACTICE_SECTOR_1	= 6,
		PRACTICE_SECTOR_2	= 7,
		PRACTICE_SECTOR_3	= 8,
		PRACTICE_LAP		= 9,
		LAST_PRACTICE
	}
}


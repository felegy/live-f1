namespace LiveF1.Core
{
	using System;

	public enum SpeedPacketType
	{
		SPEED_SECTOR1		= 1,
		SPEED_SECTOR2		= 2,
		SPEED_SECTOR3		= 3,
		SPEED_TRAP		= 4,
		FL_CAR			= 5,
		FL_DRIVER		= 6,
		FL_TIME			= 7,
		FL_LAP			= 8
	}
}


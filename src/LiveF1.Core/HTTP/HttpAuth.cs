using System.Diagnostics;

namespace LiveF1.Core.HTTP
{
	using System;
	using System.IO;
	using System.Net;
	using System.Text;

	public class HttpAuth
	{
		const string LOGIN_URL = "/reg/login";
		const string REGISTER_URL = "/reg/registration";
		const string KEY_URL_BASE = "/reg/getkey/";
		const string KEYFRAME_URL_PREFIX = "/keyframe";
		const string PACKAGE_STRING = "live-f1 0.2.11";

		static HttpAuth ()
		{

		}

		public static string ObtainAuthCookie (string host, string email, string password)
		{
			string ret = string.Empty;

			string eEmail = Uri.EscapeUriString (email);
			string ePassword = Uri.EscapeUriString (password);

			ASCIIEncoding encoding = new ASCIIEncoding ();

			string body = string.Format("email={0}&password={1}",eEmail,ePassword);
			byte[] bodyBytes = encoding.GetBytes (body);

			eEmail = null;
			ePassword = null;

			UriBuilder reqUri = new UriBuilder();
			reqUri.Scheme = Uri.UriSchemeHttp;
			reqUri.Host = host;
			reqUri.Path = LOGIN_URL;

			var loginRequest = (HttpWebRequest)WebRequest.Create(reqUri.Uri.ToString());

			loginRequest.AllowAutoRedirect = false;
			loginRequest.Method = "POST";
			loginRequest.UserAgent = PACKAGE_STRING;
			loginRequest.ContentType = "application/x-www-form-urlencoded";
			loginRequest.ContentLength = bodyBytes.Length;

			using (Stream requestStream = loginRequest.GetRequestStream())
			{
				requestStream.Write (bodyBytes, 0, bodyBytes.Length);
			}

			try 
			{    
				using (HttpWebResponse response = (HttpWebResponse)loginRequest.GetResponse())
				{
					if(response.Headers != null)
					{
						ret = response.Headers["Set-Cookie"];
					}
				}
			}
			catch (WebException wex)
			{
				var httpResponse = wex.Response as HttpWebResponse;
				if (httpResponse != null)
				{
					throw new ApplicationException(string.Format(
						"Remote server call {0} {1} resulted in a http error {2} {3}.",
						"POST",
						reqUri.Uri.ToString(),
						httpResponse.StatusCode,
						httpResponse.StatusDescription), wex);
				}
				else
				{
					throw new ApplicationException(string.Format(
						"Remote server call {0} {1} resulted in an error.",
						"POST",
						reqUri.Uri.ToString()), wex);
				}
			}
			catch (Exception)
			{
				throw;
			}

			return ret;
		}
	}
}

